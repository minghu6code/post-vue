import md5 from 'js-md5'

const RESOURCES = {
  users: 'customers',
  tokens: 'tokens',
  posts: 'posts'
}

const ERROR_CODE = {
  accessTokenExpired: 40101,
  refreshTokenInvalid: 40101 // or 40102
}

// Used for axios config
const APIS = {
  getUser: userId => {
    return {
      url: `/${RESOURCES.users}/${userId}`,
      method: 'get'
    }
  },
  createUser: (name, password) => {
    return {
      url: `/${RESOURCES.users}/`,
      method: 'post',
      data: {
        name,
        password: md5(password)
      }
    }
  },
  getToken: (name, password) => {
    return {
      url: `/${RESOURCES.tokens}/`,
      method: 'post',
      data: {
        name,
        password: md5(password)
      }
    }
  },
  refreshToken: refreshToken => {
    return {
      url: `/${RESOURCES.tokens}/refresh`,
      method: 'get',
      headers: {'Authorization': `Bearer ${refreshToken}`},
      data: {
        refreshToken
      }
    }
  },
  getPostMeta: (pageIndex, pageSize) => {
    return {
      url: `/${RESOURCES.posts}/meta`,
      method: 'get',
      params: {
        page_index: pageIndex,
        page_size: pageSize
      }
    }
  },
  getPost: postId => {
    return {
      url: `/${RESOURCES.posts}/stream/${postId}`,
      responseType: 'stream',
      method: 'get'
    }
  }
}

export { RESOURCES, ERROR_CODE, APIS }
