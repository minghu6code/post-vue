import axios from 'axios'
import { mapActions } from 'vuex'

import store from './store'
import user from '@/store/user'
import { TOKEN_PREFIX } from '@/constant/auth'
import { RESOURCES, ERROR_CODE, APIS } from '@/constant/api'
import App from '@/main'

const isGetTokenApi = config => {
  if (config.url.search(`/${RESOURCES.tokens}`) >= 0) {
    return true
  }

  return false
}

const isRefreshTokenApi = config => {
  if (config.url.search(`/${RESOURCES.tokens}/refresh`) >= 0) {
    return true
  }

  return false
}

const instance = axios.create()

// instance.defaults.timeout = 60000
instance.defaults.baseURL = 'http://127.0.0.1:8000'

instance.interceptors.request.use(config => {
  if (!isGetTokenApi(config) && !isRefreshTokenApi(config)) {
    const accessToken = user.state.accessToken
    config.headers.Authorization = TOKEN_PREFIX + accessToken

    return config
  }

  if (isRefreshTokenApi(config)) {
    const refreshToken = user.state.refreshToken
    config.headers.Authorization = TOKEN_PREFIX + refreshToken

    return config
  }

  return config
}, error => Promise.reject(error))

console.log(mapActions(['refreshSession']))

instance.interceptors.response.use(response => {
  return response
}, error => {
  const errData = error.response.data

  if (!errData || !errData.code) {
    return Promise.reject(error)
  }

  if (errData.code !== ERROR_CODE.accessTokenExpired || !user.state.refreshToken) {
    return Promise.reject(error)
  }

  if (isRefreshTokenApi(error.config)) {
    return Promise.reject(error)
  }

  // try refresh access token

  return instance
    .request(APIS.refreshToken(user.state.refreshToken))
    .then(refreshTokenResponse => {
      if (!refreshTokenResponse.data) {
        return refreshTokenResponse
      }

      const accessToken = refreshTokenResponse.data.access_token
      store.commit('refreshSession', accessToken)

      return instance.request(error.config)
    })
    .catch(error => {
      console.log(error)
      store.commit('deleteSession')
      App.$router.push({path: '/session/in'})
    })
})

export default instance
