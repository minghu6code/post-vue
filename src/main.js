// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import iView from 'iview'
import VueMarkdown from 'vue-markdown'
import 'iview/dist/styles/iview.css'
import VueRx from 'vue-rx'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription' // Disposable if using RxJS4
import { Subject } from 'rxjs/Subject' // required for domStreams option
import './lib/prism.js'
import './lib/prism.css'

Vue.config.productionTip = false

Vue.use(VueRx, {
  Observable,
  Subscription,
  Subject
})
Vue.use(iView)
Vue.component('vue-markdown', VueMarkdown)

/* eslint-disable no-new */
const vue = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})

export default vue
